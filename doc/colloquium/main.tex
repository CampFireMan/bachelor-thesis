\documentclass{f4_beamer}

\usepackage{url}
\usepackage{graphicx}
\graphicspath{{../figures/}}

\usepackage[english]{babel}
\usepackage{subfig}

\title{Colloquium}
\subtitle{Mastering the Game of Abalone using Deep Reinforcement Learning and Self-Play}
\author{Ture Claußen}
\date{\today}

\begin{document}

\section{Introduction}

\begin{frame}{Background \& Motivation}
    Games as milestones in computing:
    \begin{itemize}
        \item IBM's "DeepBlue" beating Gary Kasparov in 1996 in chess \cite{higgins_brief_2017}
        \item DeepMind's AlphaGo beating Lee Sedol in 2016 in Go \cite{deepmind_match_nodate}
    \end{itemize}
    -> Representative of two methodologies of artificial intelligence
\end{frame}

\begin{frame}{Background \& Motivation}
    Learning from scratch:
    \begin{quote}
        Instead of trying to produce a programme to simulate the adult mind, why not rather try to produce one which simulates the child's?
    \end{quote}
    - Alan Turing \cite{turing_icomputing_1950}
\end{frame}

\begin{frame}{Background \& Motivation}
    % give motivation why RL is relevant
    % mention RL is broad so not all methods here are comparable to alphazero
    (Deep) Reinforcement Learning finds many novel applications:
    \begin{itemize}
        \item Improving data center cooling at Google \cite{gamble_safety-first_2018} or content recommendation at Spotify \cite{jebara_for_2020} and Netflix \cite{siddiqi_ml_2019}.
        \item Floor planning for Google's latest TPU chip \cite{mirhoseini_graph_2021}.
        \item Robotic control (Grasping \cite{pinto_supersizing_2016,zeng_learning_2018}, poking \cite{agrawal_learning_nodate} or avoiding obstacles \cite{kahn_uncertainty-aware_2017})
        \item Very recent: Controlling/containing plasma in a tokamak fusion reactor \cite{pulsar_accelerating_nodate}
    \end{itemize}
\end{frame}


\begin{frame}{Goal of the Bachelor Thesis}
    \begin{enumerate}
        \item Application the general framework of self-play learning outlined in "Mastering the game of Go without human knowledge" to the board game of Abalone.

        \item Comparision of classical search-based methods to AlphaZero's deep reinforcement learning
    \end{enumerate}
\end{frame}

\section{Background Theory}
% Goal: Give intuition, time is not enough to explain from scratch
\begin{frame}{Artificial Intelligence: Agents}
    % rational agent: 
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{agent_environment_loop.png}
        \caption{The agent-environment interaction loop \cite[cf. p. 96]{russell_artificial_2021}}
        \label{agent_environment_loop}
    \end{figure}
\end{frame}

\begin{frame}{Artificial Intelligence: Environments}
    % use chess as example
    \begin{enumerate}
        \item Performance measure: This might be the percentage of correctly broken parts (true positives) weighed against the number of incorrectly identified parts (false positives).
        \item Environment: The conveyor belt and the parts
        \item Actuators: An arm to push the parts to a different conveyor belt
        \item Sensors: Possibly a camera, infrared sensors, etc.
    \end{enumerate}
\end{frame}

\begin{frame}{Artificial Intelligence: Environments}
    % categories help narrow down applications of different algorithms, codings etc.
    % continue to use chess as example
    Potential categorizations:
    \begin{enumerate}
        \item Fully observable vs. partially observable
        \item Discrete vs. continuous
        \item Deterministic vs. non-deterministic
        \item Episodic vs. sequential
        \item Dynamic vs. static
        \item Multi-agent vs single agent
    \end{enumerate}
\end{frame}

\begin{frame}{Adverserial Search: Game Tree \& Search Tree}
    % explain difference between game & search tree
    % exhaustive search
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{adverserial_search.png}
        \caption{A search tree of a chess position \footnote{\url{https://www.pm365.tk/products.aspx?cname=brute+force+chess&cid=26}}}
    \end{figure}
\end{frame}

\begin{frame}{Adverserial Search: Minimax}
    % goal to slash down complexity
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{minimax.png}
        \caption{Minimax for a small search tree, resulting in an utility value of 1 \cite[cf. p. 303]{russell_artificial_2021}}
        \label{minimax}
    \end{figure}
\end{frame}

\begin{frame}{Adverserial Search: Heuristic Functions}
    \begin{itemize}
        \item Problem: The number of nodes to visits grows too quickly $O(b^d)$
        \item Solution: Limit search and evaluate utility of position based on human knowledge
    \end{itemize}
\end{frame}

\begin{frame}{Adverserial Search: Heuristic Functions}
    % what constitutes a good move in go?
    \begin{itemize}
        \item Material: First, each piece is assigned an integer that represents the piece's relative value (pawn = 1, bishop/knight = 3, rook = 5, and queen = 9). Then sum the values of the pieces left on the board for each player.
        \item Space: Count the squares controlled by each player.
        \item ...
    \end{itemize}

    These evaluation functions can be combined into a linear combination of the form of:

    \begin{equation}
        h(s) = \omega_0f_0(s) + ... + \omega_nf_n(s)
    \end{equation}
\end{frame}

\begin{frame}{Adverserial Search: Alpha Beta Pruning}
    \begin{figure}
        \centering
        % \includesvg[height=7cm]{alpha_beta_pruning.svg}
        \includegraphics[height=5.5cm, keepaspectratio]{alpha_beta_pruning.png}
        \caption{The previous example but with alpha beta pruning applied. The grayed out nodes indicate, that these in fact could be pruned from the tree \cite[cf. p. 308]{russell_artificial_2021}}
        \label{alpha_beta_pruning}
    \end{figure}
\end{frame}

\begin{frame}{Adverserial Search: Conclusion}
    Yields optimal results for minimax, is limited by:
    \begin{itemize}
        \item Complexity of the environment
        \item Ability to encode knowledge into heuristic function
    \end{itemize}
\end{frame}

\begin{frame}{Adverserial Search: Monte Carlo Tree Search}
    Let uns introduce a notion of selectivity:
    \begin{itemize}
        \item Only search promising parts of the tree more deeply
        \item Iteratively build the tree
    \end{itemize}
\end{frame}

\begin{frame}{Adverserial Search: Monte Carlo Tree Search}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{monte_carlo_tree_search_selection.png}
        \caption{\textbf{Selection}: Based on formula/mechanism decide on next node to expand}
    \end{figure}
\end{frame}

\begin{frame}{Adverserial Search: Monte Carlo Tree Search}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{monte_carlo_tree_search_expansion.png}
        \caption{\textbf{Expansion}: Append children to tree}
    \end{figure}
\end{frame}

\begin{frame}{Adverserial Search: Monte Carlo Tree Search}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{monte_carlo_tree_search_simulation.png}
        \caption{\textbf{Simulation}: Simulate $n$ games until a terminal state}
    \end{figure}
\end{frame}

\begin{frame}{Adverserial Search: Monte Carlo Tree Search}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{monte_carlo_tree_search_backpropagation.png}
        \caption{\textbf{Backpropagation}: Write simulation results to parent nodes}
    \end{figure}
\end{frame}

\begin{frame}{Reinforcement Learning}
    % move away from classical methods and focus on learning
    Core components of reinforcement learning (RL):\cite[p. 1]{sutton_reinforcement_2018}
    \begin{enumerate}
        \item trial-and-error search
        \item delayed reward
    \end{enumerate}
\end{frame}

\begin{frame}{Reinforcement Learning: Markov Decision Process}
    \begin{itemize}
        \item Sequential decision making
        \item Stochastic transition between states
        \item Markov property: "he state must include information about all aspects of the past agent environment interaction that make a difference for the future" \cite[p. 48]{sutton_reinforcement_2018}
    \end{itemize}
\end{frame}

\begin{frame}{Reinforcement Learning: Markov Decision Process}
    Central items:
    \begin{enumerate}
        \item Reward signal: Scalar signal recieved after some actions. Maximization of cumul. sum is the goal
        \item Policy: A state action mapping
        \item Value function: The expected reward for a state
        \item The model: Allows for predictions of the state transitions
    \end{enumerate}
\end{frame}

\begin{frame}{Reinforcement Learning: Markov Decision Process}
    \begin{figure}
        \centering
        \includegraphics[height=4cm, keepaspectratio]{mdp_agent_environment.png}
        \caption{The markov decision process as agent-environment interaction loop \cite[cf. p. 48]{sutton_reinforcement_2018}}
        \label{mdp_agent_environment}
    \end{figure}
\end{frame}

\begin{frame}{Reinforcement Learning: $k$-armed bandit}
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{10_armed_bandit.png}
        \caption{The reward distributions of a 10-armed bandit \cite[p. 28]{sutton_reinforcement_2018}}
        \label{10_armed_bandit}
    \end{figure}
\end{frame}

\begin{frame}{Deep RL: Neural Nets}
    \begin{itemize}
        \item Neural networks are general function approximators
        \item Can be used to approximate the optimal value function
    \end{itemize}
\end{frame}

\begin{frame}{Deep RL: Neurons}
    \begin{figure}
        \centering
        \includegraphics[height=4cm, keepaspectratio]{neural_networks.png}
        \caption{The fundamental idea behind neurons}
        \label{neural_network}
    \end{figure}
\end{frame}

\begin{frame}{Deep RL: Neural Nets}
    \begin{figure}
        \centering
        \includegraphics[height=3cm, keepaspectratio]{neural_network_layers.png}
        \caption{A small neural network arranged in layers. An input layer, a hidden layer and an output layer}
        \label{neural_network_layers}
    \end{figure}
\end{frame}

\begin{frame}{Deep RL: Training}
    After defining a set of training data and the desired output for each datum:
    \begin{enumerate}
        \item Calculate the error between desired output and output of the network
        \item Calculate the gradient (the direction) for which the error gets smaller
        \item Backpropagate the gradient through the network
        \item Repeat until convergence
    \end{enumerate}
    Training data, differentiable computational graphs, gradient descent, and backpropagation as the main parts of the mechanism
\end{frame}


\begin{frame}{AlphaGo}
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{alpha_go_training.png}
        \caption{AlphaGo training pipeline \cite{silver_mastering_2016}}
    \end{figure}
\end{frame}

\begin{frame}{AlphaGo Zero \& AlphaZero}
    \begin{itemize}
        \item Simplification of AlphaGo, removes database of human expert moves
        \item Solely relies on self-play learning
        \item Performs better than the original version
        \item AlphaGo Zero is first version and AlphaZero is generalization
    \end{itemize}
\end{frame}

\begin{frame}{AlphaGo Zero: MCTS}
    \begin{figure}[!h]
        \centering
        \includegraphics[height=4cm, keepaspectratio]{alpha_zero_mcts.png}
        \caption{The MCTS \cite[p. 5]{silver_mastering_2017}}
    \end{figure}
\end{frame}

\begin{frame}{AlphaZero: Self-Play}
    \begin{figure}[!h]
        \centering
        \includegraphics[height=4.5cm, keepaspectratio]{alpha_zero_self_play.png}
        \caption{The self-play \cite[p. 5]{silver_mastering_2017}}
    \end{figure}
\end{frame}

\begin{frame}{AlphaZero: The training}
    \begin{figure}[!h]
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{alpha_zero_training.png}
        \caption{The training step with experience $(s_i, \pi_i, z)$ from self-play \cite[p. 5]{silver_mastering_2017}}
    \end{figure}
\end{frame}

\section{Abalone}

\begin{frame}{Abalone: Basic moves}
    \begin{figure}[!h]
        \centering
        \subfloat[Starting position]{
            \includegraphics[width=3.5cm, keepaspectratio]{rules_starting_position.png}
        }
        \hfill
        \subfloat["In-line" moves]{
            \includegraphics[width=3.5cm, keepaspectratio]{rules_inline_move.png}
        }
        \hfill
        \subfloat["Side-step" moves]{
            \includegraphics[width=3.5cm, keepaspectratio]{rules_side_step_move.png}
        }
        \caption{Basic moves \cite{abalone_sa_abalone_nodate}}
        \label{basics}
    \end{figure}
\end{frame}

\begin{frame}{Abalone: Basic moves}
    \begin{figure}[!h]
        \centering
        \subfloat["2-push-1" sumito]{
            \includegraphics[width=3.5cm, keepaspectratio]{rules_2-push-1_sumito.png}
        }
        \hfill
        \subfloat["3-push-1" sumito]{
            \includegraphics[width=3.5cm, keepaspectratio]{rules_3-push-1_sumito.png}
        }
        \hfill
        \subfloat["3-push-2" sumito]{
            \includegraphics[width=3.5cm, keepaspectratio]{rules_3-push-2_sumito.png}
        }
        \caption{Sumito positions allow pushing the opponent's marbles \cite{abalone_sa_abalone_nodate}}
        \label{sumito}
    \end{figure}
\end{frame}

\begin{frame}{Abalone: State of the Art}
    \begin{itemize}
        \item Search-based: Minimax with alpha beta pruning still the strongest algorithms, we use reimplementation of Tino Werner's program
        \item Learning: Abalearn was a RL based approach from 2003 that performed quite well, some smaller other projects
    \end{itemize}
\end{frame}

\section{System-Architecture}

\begin{frame}{System-Architecture: Software}
    \begin{itemize}
        \item Deep learning library: PyTorch
        \item Training framework: AlphaZero General
        \item Abalone game engine: Abalone-BoAI
    \end{itemize}
\end{frame}

\begin{frame}{System-Architecture: Parallel Training Pipeline}
    \begin{figure}
        \centering
        \includegraphics[height=6.3cm, keepaspectratio]{parallel-training.png}
        \caption{The different processes during the parallel training \cite[cf. p. 45]{bruasdal_deep_2020}}
        \label{parallel_training_pipeline}
    \end{figure}
\end{frame}

\section{Experiments \& Results}
\begin{frame}{Experiments \& Results}
    Different approaches tested:
    \begin{itemize}
        \item Running pipeline without Abalone specific adaptations
        \item Changing the reward signal
        \item Warming the network up
    \end{itemize}
\end{frame}

\begin{frame}{Experiments \& Results: Scaled naive Run}
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{performance_remote_naive_win_ratio.png}
        \caption{The win-ratio}
    \end{figure}
\end{frame}

\begin{frame}{Experiments \& Results: Scaled naive Run}
    \begin{figure}
        \centering
        \includegraphics[height=5.5cm, keepaspectratio]{performance_remote_naive_reward.png}
        \caption{The cumulative reward}
    \end{figure}
\end{frame}

\begin{frame}{Experiments \& Results: Run with adjusted Reward}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{performance_remote_diff_z_win_ratio.png}
        \caption{The win-ratio}
    \end{figure}
\end{frame}

\begin{frame}{Experiments \& Results: Run with adjusted Reward}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{performance_remote_diff_z_cumul_reward.png}
        \caption{The cumulative reward}
    \end{figure}
\end{frame}

\begin{frame}{Experiments \& Results: Warmed-up Run}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{performance_remote_warmed_win_ratio.png}
        \caption{The win-ratio}
    \end{figure}
\end{frame}

\begin{frame}{Experiments \& Results: Warmed-up Run}
    \begin{figure}
        \centering
        \includegraphics[height=5cm, keepaspectratio]{performance_remote_warmed_cumul_reward.png}
        \caption{The cumulative reward}
    \end{figure}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion: Findings}
    \begin{itemize}
        \item Training iterations took too long
        \item No strong convergence visible
        \item Weak playing performance
    \end{itemize}
\end{frame}

\begin{frame}{Conclusion: Outlook}
    \begin{itemize}
        \item Improve bottlenecks to speed up training
        \item Use larger neural network for warmed-up network
        \item Test different reward signals
    \end{itemize}
\end{frame}

\section{Appendix}
\begin{frame}{Appendix}
    Slides and all other material available under:
    \url{https://github.com/campfireman/bachelor-thesis}
\end{frame}

\bibliographystyle{../lib/IEEEtran}
\bibliography{../ref}

\end{document}
